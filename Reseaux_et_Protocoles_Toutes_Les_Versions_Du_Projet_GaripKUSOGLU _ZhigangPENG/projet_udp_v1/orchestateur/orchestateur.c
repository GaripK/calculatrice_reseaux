#include <stdio.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <sys/socket.h>
#include <string.h>
#include <time.h>
#include <stdlib.h>
#define addPORT 8886
#define minPORT 8887
#define mulPORT 8888
#define divPORT 8889
#define REMOTEIP "::1"

static void aff()
{
	time_t now ;
	time (&now);
	printf("orchestrateur>%s",ctime(&now));
}
int main()
{
	  srand(time(NULL));
    int mysocket,len;
    struct sockaddr_in6 addr;
    int addr_len;
    char msg[200];
    if((mysocket=socket(AF_INET6,SOCK_DGRAM,0))<0)
    {
        perror("error: socket not created\n");
        return(1);
    }
    else
    {
				printf("creating socket ...\n");
				sleep(rand()%6);
        printf("socket created ...\n");

    }

    addr_len=sizeof(struct sockaddr_in6);
    bzero(&addr,sizeof(addr));
    addr.sin6_family=AF_INET6;
    inet_pton(AF_INET6,REMOTEIP,&addr.sin6_addr);

	char op;
	time_t start, end;
    while(1)
    {
		aff();
        bzero(msg,sizeof(msg));
        len=read(STDIN_FILENO,msg,sizeof(msg));

		if(len ==0)
		{
			printf("error: calcul is empty\n");
			exit(-1);
		}

		if(strncmp(msg,"exit",4)==0)
		{
			return(0);
		}
		printf("waiting from socket ...\n" );
		sleep(rand()%1);//a mettre a 51
		op = msg[0];


		if(op=='+')
		{
			addr.sin6_port=htons(addPORT);
		}
		if(op=='-')
		{
			addr.sin6_port=htons(minPORT);
		}
		if(op=='*')
		{
			addr.sin6_port=htons(mulPORT);
		}
		if(op=='/')
		{
			addr.sin6_port=htons(divPORT);
		}
		int j=0;
		while(msg[j]!='\n')
			j++;
		msg[j]='\0';
		start = time(NULL);
        if((sendto(mysocket,msg,sizeof(msg),0,(struct sockaddr *)&addr,addr_len))<0)
        {
            printf("error: can not send to socket\n");
            return(1);
        }

        if((recvfrom(mysocket,msg,sizeof(msg),0,(struct sockaddr *)&addr,(socklen_t*)&addr_len))<0)
				{
					printf("socket unaviable. restarting ...\n");
					break;
				}
		end = time(NULL);
		if(difftime(end, start)>20)
			printf("le nœud de %c n’est plus disponible",op);

        printf("\n");
        printf("Received message : %s\n",msg);
    }
}
