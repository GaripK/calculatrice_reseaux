#include <unistd.h>
#include <stdio.h>
#include <arpa/inet.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <string.h>
#include <time.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <sys/stat.h>
#include <fcntl.h>

//fonction affichant la date et l'heure
static void aff()
{
	time_t now;
	time (&now);
	printf("%s",ctime(&now));
}

int main(int argc, char **argv)
{
//initialisation des ports et de l'adresse
int addPORT = 6001;
int minPORT = 6002;
int mulPORT = 6003;
int divPORT = 6004;
char REMOTEIP[300] = "turing6.u-strasbg.fr";
	if(argc != 6)
	{
		printf("USAGE: %s REMOTEIP addPORT minPORT mulPORT divPORT\n", argv[0]);
		printf("orchestrateur> proceding with default ip and port\n");
	}
	else
	{
		//affectation des arguments en paramètres aux ports et à l'adresse
		strcpy(REMOTEIP,argv[1]);
		addPORT = atoi(argv[2]);
		minPORT = atoi(argv[3]);
		mulPORT = atoi(argv[4]);
		divPORT = atoi(argv[5]);
	}

	//initialistation des variables
	srand(time(NULL));
	int mysocket;
	int mysocket2;
	struct sockaddr_in6 addr;
	struct sockaddr_in addr2;
	int addr_len;
	int addr_len2;
	char msg[200];
	char msg2[200][4][200];
	char msgerr[4][200][200];
	char sendtime1[26];
	char recvtime2[26];
	int port;
	int whatip=0;
	char ipv6[200];
	char ipv4[200];
	char op;
	int iop;
	int len;
	int i;
	int i2;
	int boolean;
	int boolean2 ;
	unsigned int init;
	int test;
	int k;
	int f;
	struct timeval read_timeout;
	read_timeout.tv_sec = 1;
	read_timeout.tv_usec = 0;

	//creation des sockets ipv4 et ipv6
	if((mysocket=socket(AF_INET6,SOCK_DGRAM,0))<0)
	{
    perror("error: ipv6 socket not created\n");
    return(1);
	}
	else
	{
		//affichage de la creation de la socket ipv6
		printf("orchestrateur> creating ipv6 socket ...\n");
		sleep((rand()%2)+1);
    printf("orchestrateur> ipv6 socket created ...\n");

	}
	if((mysocket2=socket(AF_INET,SOCK_DGRAM,0))<0)
	{
    perror("error: ipv4 socket not created\n");
    return(1);
	}
	else
	{
		//affichage de la creation de la socket ipv4
		printf("orchestrateur> creating ipv4 socket ...\n");
		sleep((rand()%2)+1);
    printf("orchestrateur> ipv4 socket created ...\n");

	}

	//convertion de l'adresse en ipv4 et ipv6
	addr_len=sizeof(struct sockaddr_in6);
	bzero(&addr,sizeof(addr));
	addr.sin6_family=AF_INET6;
	inet_pton(AF_INET6,REMOTEIP,&addr.sin6_addr);

	addr_len2=sizeof(struct sockaddr_in);
	bzero(&addr2,sizeof(addr2));
	addr2.sin_family=AF_INET;
	inet_pton(AF_INET,REMOTEIP,&addr2.sin_addr);

	//recupération de l'adresse ipv6 et ipv4
	inet_ntop(AF_INET6, &addr.sin6_addr, ipv6, 200);
	inet_ntop(AF_INET, &addr2.sin_addr, ipv4, 200);

	//affichage initial de la commande 'help'
	printf(" ---------- help ---------- \n");
	printf(" press enter to validate the line\n");
	printf(" press enter with an empty line to validate and start processing\n");
	printf(" write 'exit' to exit\n");
	printf(" write '+(a, b, ...)' to make an addition\n");
	printf(" write '-(a, b, ...)' to make a substraction\n");
	printf(" write '*(a, b, ...)' to make a multiplication\n");
	printf(" write '/(a, b, ...)' to make a division\n");
	printf(" write 'help' to see this help again\n\n");

	//initialisation de la boucle de commande(s)/réponse(s)
  while(1)
  {
		//affichage de la date à chaque début de boucle
		aff();
		//réinitialisation des variables
		i=0;
		i2=0;
		boolean = 0;

		//initialisation de la boucle de lecture de commande(s)
		while((write(STDIN_FILENO,"orchestrateur> ",sizeof("orchestrateur> "))!=0))
		{
			boolean2 = 0;
			//réinitialisation du msg lut
	    bzero(msg,sizeof(msg));
			//lecture de commande(s)
			len=read(STDIN_FILENO,msg,sizeof(msg));

			//fin des commande si on appuis sur enter avec une ligne vide
			if(len<=1)
			{
				break;
			}

			//fin du programme si on tape exit
			if(strncmp(msg,"exit",4)==0)
			{
				close(mysocket);
				close(mysocket2);
				printf("orchestrateur> ---------- exit ---------- \n");
				return(0);
			}

			//affichage de l'aide puis reboucle sur l'entré de commande
			if(strncmp(msg,"help",4)==0)
			{
				printf(" ---------- help ---------- \n");
				printf(" press enter to validate the line\n");
				printf(" press enter with an empty line to validate and start processing\n");
				printf(" write 'exit' to exit\n");
				printf(" write '+(a, b, ...)' to make an addition\n");
				printf(" write '-(a, b, ...)' to make a substraction\n");
				printf(" write '*(a, b, ...)' to make a multiplication\n");
				printf(" write '/(a, b, ...)' to make a division\n");
				printf(" write 'help' to see this help again\n");
				continue;
			}

			//condition si la commande est trop longue
			if(len>=200)
			{
				printf("the requests is too long\n");
				continue;
			}

			//condition si un trop grand nombre de commande est entré
			if(i<200)
			{
				//boucle qui efface le carctère de saut de ligne obligatoire à la lecture
				int j=0;
				while(msg[j]!='\n')
					j++;
				msg[j]='\0';

				//test si il y a un des calcules imbriqués
				test=0;
				while(init<sizeof(msg)-1)
				{
					if(((msg[init]=='+')||(msg[init]=='-')||(msg[init]=='*')||(msg[init]=='/'))&&(msg[init+1]=='('))
					{
						test++;
						if(test>1)
						{
							//si oui affichage correspondant et rebouclage
							boolean2=1;
							printf("orchestrateur> too much requests in one\n");
							break;
						}
					}
					init++;
				}
				if(boolean2!=1)
				{
					//sauvegarde des messages envoyable aux noeuds
					op = msg[0];
					if(op=='+')
					{
						strcpy(msg2[i][0],msg);
						i++;
					}
					if(op=='-')
					{
						strcpy(msg2[i][1],msg);
						i++;
					}
					if(op=='*')
					{
						strcpy(msg2[i][2],msg);
						i++;
					}
					if(op=='/')
					{
						strcpy(msg2[i][3],msg);
						i++;
					}
				}
				boolean=0;
			}
			else
			{
				//si un trop grand nombre de commandes est entré
				printf("orchestrateur> Too much requests, processing all but not %s ...\n",msg);
				break;
			}
			bzero(msg,sizeof(msg));
		}

		//vérifie qu'il y a au moins une commande pour aficher :
		boolean=0;
		for(k=0;k<4;k++)
		{
			if(strcmp(msg2[0][k],"")!=0)
				boolean=1;
		}
		if(boolean==1)
		{
			//pour afficher ceci
			printf("orchestrateur> ---------- processing request(s) ---------- \n");
		}
		boolean=0;

		//boucle d'affichage dans le cas ou un noeud ipv4 ou ipv6 existe
		for(k=i-1;k>-1;k--)
		{
			//creation des ports
			if(strcmp(msg2[k][0],"")!=0)
			{
				op='+';
				iop=0;
				addr.sin6_port=htons(addPORT);
				addr2.sin_port=htons(addPORT);
				port=addPORT;
			}
			if(strcmp(msg2[k][1],"")!=0)
			{
				op='-';
				iop=1;
				addr.sin6_port=htons(minPORT);
				addr2.sin_port=htons(minPORT);
				port=minPORT;
			}
			if(strcmp(msg2[k][2],"")!=0)
			{
				op='*';
				iop=2;
				addr.sin6_port=htons(mulPORT);
				addr2.sin_port=htons(mulPORT);
				port=mulPORT;
			}
			if(strcmp(msg2[k][3],"")!=0)
			{
				op='/';
				iop=3;
				addr.sin6_port=htons(divPORT);
				addr2.sin_port=htons(divPORT);
				port=divPORT;
			}

			//affichage de l'envoi de la commande
			printf("orchestrateur> sending %s to socket ...\n", msg2[k][iop] );
			time_t sendnow;
			time (&sendnow);
			strcpy(sendtime1,ctime(&sendnow)); // sauvegarde de la date de debut pour ensuite afficher uniquement l'heure

			//fonction permettant l'ajout d'un timer sur une socket ce qui permet la résolution de la reception bloquante
			//lorsqu'un noeud n'est pas disponible
			setsockopt(mysocket,SOL_SOCKET,SO_RCVTIMEO, &read_timeout,sizeof read_timeout);

			//envoi de la commande
			if((sendto(mysocket,msg2[k][iop],sizeof(msg2[k][iop]),0,(struct sockaddr *)&addr,addr_len))<0)
			{
				boolean=1;
			}

			//attente de reception simulé
			sleep((rand()%15)+1);

			//reception de la réponse
			if((recvfrom(mysocket,msg2[k][iop],sizeof(msg2[k][iop]),0,(struct sockaddr *)&addr,(socklen_t*)&addr_len))<0)
			{
				printf("orchestrateur> the ipv6 node '%c' is not available\n",op);
				printf("orchestrateur> trying the ipv4 node...\n");
				boolean=1;
			}
			else
			{
				printf("orchestrateur> the ipv6 node '%c' is available\n",op);
				printf("orchestrateur> not trying the ipv4 node\n");
				whatip=6;
			}

			//même envoi/reception en ipv4 (uniquement si ipv6 echoue)
			if(boolean==1)
			{
				boolean=0;
				time_t sendnow3;
				time (&sendnow3);
				strcpy(sendtime1,ctime(&sendnow3));
				setsockopt(mysocket2,SOL_SOCKET,SO_RCVTIMEO, &read_timeout,sizeof read_timeout);
				if((sendto(mysocket2,msg2[k][iop],sizeof(msg2[k][iop]),0,(struct sockaddr *)&addr2,addr_len2))<0)
				{
					printf("orchestrateur> the ipv4 node '%c' is not available\n",op);
					printf("orchestrateur> proceding without the missing node...\n");
					boolean=1;
				}
				sleep((rand()%15)+1);
				if((recvfrom(mysocket2,msg2[k][iop],sizeof(msg2[k][iop]),0,(struct sockaddr *)&addr2,(socklen_t*)&addr_len2))<0)
				{
					printf("orchestrateur> the ipv4 node '%c' is not available\n",op);
					printf("orchestrateur> proceding without the missing node...\n");
					boolean=1;
				}
				else
				{
					printf("orchestrateur> the ipv4 node '%c' is available\n",op);
					whatip=4;
				}

			}

			//affichage de la réception
			if(boolean==0)
			{
				time_t recvnow;
				time (&recvnow);
				strcpy(recvtime2,ctime(&recvnow));//sauvegarde de la date de fin

				//garde unique les heures de début et de fin
				char str[26];
				char str2[26];
				strncpy(str, sendtime1 + 11, 8);
				str[8]='\0';
				strncpy(str2, recvtime2 + 11, 8);
				str2[8]='\0';

				//affichage de la réception en fonction du protocole ip utilisé
		    printf("orchestrateur> Received message :\n");
				if(whatip==6)
					printf("sended at %s by <%s,%d> recieved at %s\n",str,ipv6,port,str2);
				else if (whatip==4)
					printf("sended at %s by <%s,%d> recieved at %s\n",str,ipv4,port,str2);
				printf("orchestrateur> %s\n",msg2[k][iop]);
			}

			//sauvegarde des calcules non renvoyé par les noeuds (noeuds indisponibles)
			else
			{
				strcpy(msgerr[iop][i2],msg2[k][iop]);
				i2++;
			}
		}
		//affichage résumant les calcules non envoyé
		if(i2>0)
		{
			for(f=0;f<4;f++)
			{
				if(strcmp(msgerr[f][0],"")!=0)
				{
					if(f==0)
						op='+';
					if(f==1)
						op='-';
					if(f==2)
						op='*';
					if(f==3)
						op='/';
					printf("orchestrateur> The node '%c' is missing, the following message(s) has not been send :\n",op);
					for(k=i2-1;k>-1;k--)
					{
						printf("orchestrateur> %s\n", msgerr[f][k]);
					}
				}
			}
		}
		printf("\n");

		//réinitialisation de la pile de méssages
		for(k=0;k<i;k++)
		{
			for(f=0;f<4;f++)
				bzero(msg2[k][f],sizeof(msg2[k][f]));
		}
		for(f=0;f<4;f++)
		{
			for(k=0;k<i2;k++)
				bzero(msgerr[f][k],sizeof(msgerr[f][k]));
		}

		sleep((rand()%2)+1);
	}
	close(mysocket);
	close(mysocket2);
}
