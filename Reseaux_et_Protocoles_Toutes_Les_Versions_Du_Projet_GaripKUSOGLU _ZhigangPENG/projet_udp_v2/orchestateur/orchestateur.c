#include <stdio.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <string.h>
#include <time.h>
#include <stdlib.h>
#define addPORT 8886
#define minPORT 8887
#define mulPORT 8888
#define divPORT 8889
#define REMOTEIP "::1"

static void aff()
{
	time_t now ;
	time (&now);
	printf("%s",ctime(&now));
}
int main()
{
	  srand(time(NULL));
    int mysocket;
    struct sockaddr_in6 addr;
    int addr_len;
    char msg[200];
		char msg2[200][200];

    if((mysocket=socket(AF_INET6,SOCK_DGRAM,0))<0)
    {
        perror("error: socket not created\n");
        return(1);
    }
    else
    {
				printf("orchestrateur> creating socket ...\n");
				sleep(rand()%1);
        printf("orchestrateur> socket created ...\n");

    }

    addr_len=sizeof(struct sockaddr_in6);
    bzero(&addr,sizeof(addr));
    addr.sin6_family=AF_INET6;
    inet_pton(AF_INET6,REMOTEIP,&addr.sin6_addr);

	char op;
	time_t start, end;
	int len;
    while(1)
    {
			aff();
        bzero(msg,sizeof(msg));
			int i=0;
			while((write(STDIN_FILENO,"orchestrateur> ",sizeof("orchestrateur> "))!=0))
			{
				len=read(STDIN_FILENO,msg,sizeof(msg));
				if(len<=1)
				{
					break;
				}
				if(strncmp(msg,"exit",4)==0)
				{
					return(0);
				}
				op = msg[0];


				if(op=='+')
				{
					addr.sin6_port=htons(addPORT);
				}
				if(op=='-')
				{
					addr.sin6_port=htons(minPORT);
				}
				if(op=='*')
				{
					addr.sin6_port=htons(mulPORT);
				}
				if(op=='/')
				{
					addr.sin6_port=htons(divPORT);
				}
				int j=0;
				while(msg[j]!='\n')
					j++;
				msg[j]='\0';
				start = time(NULL);
		        if((sendto(mysocket,msg,sizeof(msg),0,(struct sockaddr *)&addr,addr_len))<0)
		        {
		            printf("error: can not send to socket\n");
		            return(1);
		        }

		        if((recvfrom(mysocket,msg,sizeof(msg),0,(struct sockaddr *)&addr,(socklen_t*)&addr_len))<0)
						{
							printf("socket unaviable. restarting ...\n");
							break;
						}
				end = time(NULL);

				strcpy(msg2[i],msg);
				i++;
				if(difftime(end, start)>20)
					printf("le nœud de %c n’est plus disponible",op);
				bzero(msg,sizeof(msg));
			}
				int k;
				for(k=0;k<i;k++)
				{
					printf("orchestrateur> waiting from socket ...\n" );
					sleep(rand()%1);//a mettre a 51
		      printf("orchestrateur> Received message : %s\n",msg2[k]);
				}
				printf("\n");

				for(k=0;k<i;k++)
				bzero(msg2[k],sizeof(msg2[k]));
    }
}
