#!/usr/bin/env bash

cd orchestateur
make mrproper && make

cd ../noede/add
make mrproper && make

cd ../div
make mrproper && make

cd ../min
make mrproper && make

cd ../mul
make mrproper && make

cd ../..

evalBack() { eval "$@" &>/dev/null &disown; }

evalBack ./noede/add/noede
evalBack ./noede/div/noede
evalBack ./noede/min/noede
evalBack ./noede/mul/noede

./orchestateur/orchestateur

killall -9 noede
