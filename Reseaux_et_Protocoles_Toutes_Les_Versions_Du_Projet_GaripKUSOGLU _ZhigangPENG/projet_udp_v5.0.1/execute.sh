#!/usr/bin/env bash

cd orchestrateur
make mrproper && make

cd ../node/add
make mrproper && make

cd ../div
make mrproper && make

cd ../min
make mrproper && make

cd ../mul
make mrproper && make

cd ../..

evalBack() { eval "$@" &>/dev/null &disown; }

evalBack ./node/add/node_add
evalBack ./node/div/node_div
evalBack ./node/min/node_min
evalBack ./node/mul/node_mul

./orchestrateur/orchestrateur

killall -9 node_add
killall -9 node_div
killall -9 node_min
killall -9 node_mul
