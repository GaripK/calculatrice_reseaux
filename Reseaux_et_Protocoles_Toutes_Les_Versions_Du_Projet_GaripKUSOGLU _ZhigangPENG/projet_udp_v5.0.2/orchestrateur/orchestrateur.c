#include <stdio.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <string.h>
#include <time.h>
#include <stdlib.h>
#define addPORT 8886
#define minPORT 8887
#define mulPORT 8888
#define divPORT 8889
#define REMOTEIP "::1"

static void aff()
{
	time_t now;
	time (&now);
	printf("%s",ctime(&now));
}
int main()
{
	srand(time(NULL));
	int mysocket;
	struct sockaddr_in6 addr;
	int addr_len;
	char msg1[200][200];
	char msg[200];
	char msg2[200][4][200];
	char msgerr[4][200][200];
	char sendtime1[26];
	char recvtime2[26];
	int port;

	if((mysocket=socket(AF_INET6,SOCK_DGRAM,0))<0)
	{
	    perror("error: socket not created\n");
	    return(1);
	}
	else
	{
			printf("orchestrateur> creating socket ...\n");
			sleep(rand()%1);
	    printf("orchestrateur> socket created ...\n");

	}

	addr_len=sizeof(struct sockaddr_in6);
	bzero(&addr,sizeof(addr));
	addr.sin6_family=AF_INET6;
	inet_pton(AF_INET6,REMOTEIP,&addr.sin6_addr);

	char op;
	int iop;
	int len;
	struct timeval read_timeout;
	read_timeout.tv_sec = 0;
	read_timeout.tv_usec = 10;
	printf("orchestrateur> press enter to validate the line\n");
	printf("orchestrateur> press enter with an empty line to validate and start processing\n");
	printf("orchestrateur> write exit to exit\n");
  while(1)
  {
		aff();
      bzero(msg,sizeof(msg));
		int i=0;
		int i2=0;
		int boolean = 0;
		while((write(STDIN_FILENO,"orchestrateur> ",sizeof("orchestrateur> "))!=0))
		{
			len=read(STDIN_FILENO,msg,sizeof(msg));
			if(len<=1)
			{
				break;
			}
			if(strncmp(msg,"exit",4)==0)
			{
				printf("orchestrateur> exit...\n");
				return(0);
			}

			if(i<200)
			{
				int j=0;
				while(msg[j]!='\n')
					j++;
				msg[j]='\0';
				//NEW CODE
				int test=0,decompose,decompose2;
				unsigned int init,end[10]={0},start[10]={0};
				while(init<sizeof(msg)-1)
				{
					if(((msg[init]=='+')||(msg[init]=='-')||(msg[init]=='*')||(msg[init]=='/'))&&(msg[init+1]=='('))
					{
						if(test<200)
						{
							test++;
							if(test>1)
								boolean=1;
						}
						else
						{
							printf("orchestrateur> too much requests in one\n");
							boolean=2;
							break;
						}
					}
					init++;
				}
				test--;
				if(boolean==2)
					break;
				printf("%d\n",test );
				if(boolean==1)
				{
					decompose=-1;
					decompose2=-1;
					init=0;
					int a=0;
					while(a<1)
					{
						if(msg[init]=='(')
						{
							a++;
							init++;
						}
						init++;
					}
					printf("%d\n",init );
					while((decompose2<test)&&(init<sizeof(msg)-1))
					{
						if(msg[init]=='(')
						{
							decompose++;
							start[decompose]=init;
							start[decompose]--;
						}
						if(msg[init]==')')
						{
							decompose2++;
							end[decompose2]=init;
						}
						init++;
					}
					for(a=0;a<test;a++)
						printf("a:%d %d\n",start[a],end[a]);
					int boolean2=0;
					for(a=0;a<test;a++)
					{
						if(end[a]>start[(a+1)%test])
						{
							boolean2=1;
							break;
						}
					}
					if(boolean2==1)
					{
						int b=0;
						while((b<test)&&(end[b]<start[(b+1)%test]))
						{
						 b++;
					 	}
						printf("%d\n", b);
						int c,d=0;
						while((b<test)&&(end[b]>start[(b+1)%test])&&(end[b]<end[(b+1)%test]))
						{
							int tmp=end[test-1-d];
							for(c=test-1;c>b;c--)
							{
								if((end[c]>start[(c+1)%test])&&(end[c]<end[(c+1)%test]))
								{
									end[c]=end[c-1];
									d++;
								}
							}
							end[b]=tmp;
							b++;
						}
					}
					for(a=0;a<test;a++)
						printf("b:%d %d\n",start[a],end[a]);
					for(decompose=0;decompose<test;decompose++)
					{
						strncpy(msg1[decompose],msg + start[decompose],end[decompose]-start[decompose]+1);
					}
					for(decompose=0;decompose<test;decompose++)
					{
						printf("%s\n",msg1[decompose]);//essai d'affichage du calcul décomposé
						bzero(msg1[decompose],sizeof(msg1[decompose]));
					}
				}
				//END NEW CODE
				else
				{
					op = msg[0];
					if(op=='+')
					{
						strcpy(msg2[i][0],msg);
						i++;
					}
					if(op=='-')
					{
						strcpy(msg2[i][1],msg);
						i++;
					}
					if(op=='*')
					{
						strcpy(msg2[i][2],msg);
						i++;
					}
					if(op=='/')
					{
						strcpy(msg2[i][3],msg);
						i++;
					}
				}
				boolean=0;
			}
			else
			{
				printf("orchestrateur> Too much requests, processing all but not %s ...\n",msg);
				break;
			}
			bzero(msg,sizeof(msg));
		}
		int k;
		int f;
		for(k=0;k<4;k++)
		{
			if(strcmp(msg2[0][k],"")!=0)
				boolean=1;
		}
		if(boolean==1)
		{
			printf("orchestrateur> processing request(s) ...\n");
		}
		boolean=0;
		for(k=i-1;k>-1;k--)
		{

			if(strcmp(msg2[k][0],"")!=0)
			{
				op='+';
				iop=0;
				addr.sin6_port=htons(addPORT);
				port=addPORT;
			}
			if(strcmp(msg2[k][1],"")!=0)
			{
				op='-';
				iop=1;
				addr.sin6_port=htons(minPORT);
				port=minPORT;
			}
			if(strcmp(msg2[k][2],"")!=0)
			{
				op='*';
				iop=2;
				addr.sin6_port=htons(mulPORT);
				port=mulPORT;
			}
			if(strcmp(msg2[k][3],"")!=0)
			{
				op='/';
				iop=3;
				addr.sin6_port=htons(divPORT);
				port=divPORT;
			}
			printf("orchestrateur> sending %s to socket ...\n", msg2[k][iop] );
			time_t sendnow;
			time (&sendnow);
			strcpy(sendtime1,ctime(&sendnow));
			setsockopt(mysocket,SOL_SOCKET,SO_RCVTIMEO, &read_timeout,sizeof read_timeout);
			if((sendto(mysocket,msg2[k][iop],sizeof(msg2[k][iop]),0,(struct sockaddr *)&addr,addr_len))<0)
			{
				printf("orchestrateur> The node '%c' is no longer available\n",op);
				printf("orchestrateur> proceding without the missing node...\n");
				boolean=1;
			}
			sleep(rand()%3);
			if((recvfrom(mysocket,msg2[k][iop],sizeof(msg2[k][iop]),0,(struct sockaddr *)&addr,(socklen_t*)&addr_len))<0)
			{
				printf("orchestrateur> The node '%c' is no longer available\n",op);
				printf("orchestrateur> proceding without the missing node...\n");
				boolean=1;
			}
			if(boolean==0)
			{
				time_t recvnow;
				time (&recvnow);
				strcpy(recvtime2,ctime(&recvnow));
				char str[26];
				char str2[26];
				strncpy(str, sendtime1 + 11, 8);
				str[8]='\0';
				strncpy(str2, recvtime2 + 11, 8);
				str2[8]='\0';
		    printf("orchestrateur> Received message :\n");
				printf("sended at %s by <%s,%d> recieved at %s\n",str,REMOTEIP,port,str2);
				printf("orchestrateur> %s\n",msg2[k][iop]);
			}
			else
			{
				strcpy(msgerr[iop][i2],msg2[k][iop]);
				i2++;
			}
		}

		if(i2>0)
		{
			for(f=0;f<4;f++)
			{
				if(strcmp(msgerr[f][0],"")!=0)
				{
					if(f==0)
						op='+';
					if(f==1)
						op='-';
					if(f==2)
						op='*';
					if(f==3)
						op='/';
					printf("orchestrateur> The node '%c' is missing, the following message(s) has not been send :\n",op);
					for(k=i2-1;k>-1;k--)
					{
						printf("orchestrateur> %s\n", msgerr[f][k]);
					}
				}
			}
		}
		printf("\n");

		for(k=0;k<i;k++)
		{
			for(f=0;f<4;f++)
				bzero(msg2[k][f],sizeof(msg2[k][f]));
		}
		for(f=0;f<4;f++)
		{
			for(k=0;k<i2;k++)
				bzero(msgerr[f][k],sizeof(msgerr[f][k]));
		}

		sleep(rand()%2);
	}
}
