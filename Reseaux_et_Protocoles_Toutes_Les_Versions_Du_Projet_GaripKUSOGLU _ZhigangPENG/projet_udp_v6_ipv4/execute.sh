#!/usr/bin/env bash

cd orchestrateur
make mrproper && make

cd ../node/add
make mrproper && make

cd ../div
make mrproper && make

cd ../min
make mrproper && make

cd ../mul
make mrproper && make

cd ../../nodeipv4/add
make mrproper && make

cd ../div
make mrproper && make

cd ../min
make mrproper && make

cd ../mul
make mrproper && make

cd ../..

evalBack() { eval "$@" &>/dev/null &disown; }

if [ $(( RANDOM % 2 )) -eq 0 ]
then
  evalBack ./node/add/node_add
else
  evalBack ./nodeipv4/add/node_add
fi

if [ $(( RANDOM % 2 )) -eq 0 ]
then
  evalBack ./node/div/node_div
else
  evalBack ./nodeipv4/div/node_div
fi

if [ $(( RANDOM % 2 )) -eq 0 ]
then
  evalBack ./node/min/node_min
else
  evalBack ./nodeipv4/min/node_min
fi

if [ $(( RANDOM % 2 )) -eq 0 ]
then
  evalBack ./node/mul/node_mul
else
  evalBack ./nodeipv4/mul/node_mul
fi

./orchestrateur/orchestrateur

killall -9 node_add
killall -9 node_div
killall -9 node_min
killall -9 node_mul
