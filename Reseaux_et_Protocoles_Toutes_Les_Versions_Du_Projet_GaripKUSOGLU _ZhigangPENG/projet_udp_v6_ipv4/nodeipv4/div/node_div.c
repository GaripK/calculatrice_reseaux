#include <unistd.h>
#include <stdio.h>
#include <arpa/inet.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <string.h>
#include <time.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <limits.h>
#define LOCALPORT 8889
int main()
{
  int mysocket,len;
  int num,res;

  struct sockaddr_in addr;
  int addr_len,i=0,q;
  char msg[200];
  char buf[300];

  if((mysocket=socket(AF_INET,SOCK_DGRAM,0))<0)
  {
    perror("error:");
    return(1);
  }
  else
  {
    printf("socket created ...\n");
    printf("socket id :%d \n",mysocket);
  }

  addr_len=sizeof(struct sockaddr_in);
  bzero(&addr,sizeof(addr));
  addr.sin_family=AF_INET;
  addr.sin_port=htons(LOCALPORT);
  addr.sin_addr.s_addr = htonl(INADDR_ANY);

  if(bind(mysocket,(struct sockaddr *)&addr,sizeof(addr))<0)
  {
      perror("connect");
      return(1);
  }
  else
  {
      printf("bind ok .\n");
      printf("local port : %d\n",LOCALPORT);
  }
  while(1)
  {
    i=0;
    bzero(msg,sizeof(msg));
    len = recvfrom(mysocket,msg,sizeof(msg),0,(struct sockaddr *)&addr,(socklen_t*)&addr_len);
    printf("reading numbers...");
    while(msg[i]&&(msg[i]<'0' || msg[i]>'9' ))
      i++;
    if(msg[i])
    {
      //p=i;
      q=i+1;
      res=msg[i]-'0';
      if(i>=1 && msg[i-1]=='-')
        res=-res;
      while(msg[q]>='0' && msg[q]<='9')
      {
        res=10*res+(msg[q]-'0');
        q++;
      }
      i=q;
    }
    while(1)
    {
      while(msg[i]&&(msg[i]<'0' || msg[i]>'9' ))
        i++;
      if(msg[i])
      {
        //p=i;
        q=i+1;
        num=msg[i]-'0';
        if(i>=1 && msg[i-1]=='-')
          num=-num;
        while(msg[q]>='0' && msg[q]<='9')
        {
          num=10*num+(msg[q]-'0');
          q++;
        }
        if(num==0)
        {
          strcpy(msg,"Error division by 0");
          res=INT_MAX;
          break;
        }
        i=q;
      }
      else
        break;
      res/=num;
    }
    if(res!=INT_MAX)
      sprintf(msg, "%s = %d",msg,res);
    inet_ntop(AF_INET,&addr.sin_addr,buf,sizeof(buf));
    printf("message from ip %s\n",buf);
    printf("Received message : %s\n",msg);
    if(sendto(mysocket,msg,len,0,(struct sockaddr *)&addr,addr_len)<0)
    {
        printf("error");
        return(1);
    }
  }
}
