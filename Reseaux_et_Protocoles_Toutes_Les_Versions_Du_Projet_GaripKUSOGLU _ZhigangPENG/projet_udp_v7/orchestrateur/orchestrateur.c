#include <unistd.h>
#include <stdio.h>
#include <arpa/inet.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <string.h>
#include <time.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <sys/stat.h>
#include <fcntl.h>

static void aff()
{
	time_t now;
	time (&now);
	printf("%s",ctime(&now));
}
int main(int argc, char **argv)
{
int addPORT = 8886;
int minPORT = 8887;
int mulPORT = 8888;
int divPORT = 8889;
char REMOTEIP[300] = "::1";
	if(argc != 6)
	{
		printf("USAGE: %s REMOTEIP addPORT minPORT mulPORT divPORT\n", argv[0]);
		printf("orchestrateur> proceding with default ip and port\n");
	}
	else
	{
		strcpy(REMOTEIP,argv[1]);
		addPORT = atoi(argv[2]);
		minPORT = atoi(argv[3]);
		mulPORT = atoi(argv[4]);
		divPORT = atoi(argv[5]);
	}
	srand(time(NULL));
	int mysocket;
	int mysocket2;
	struct sockaddr_in6 addr;
	struct sockaddr_in addr2;
	int addr_len;
	int addr_len2;
	char msg[200];
	char msg2[200][4][200];
	char msgerr[4][200][200];
	char sendtime1[26];
	char recvtime2[26];
	int port;

	if((mysocket=socket(AF_INET6,SOCK_DGRAM,0))<0)
	{
	    perror("error: socket not created\n");
	    return(1);
	}
	else
	{
			printf("orchestrateur> creating ipv6 socket ...\n");
			sleep(rand()%2);
	    printf("orchestrateur> ipv6 socket created ...\n");

	}
	if((mysocket2=socket(AF_INET,SOCK_DGRAM,0))<0)
	{
	    perror("error: socket not created\n");
	    return(1);
	}
	else
	{
			printf("orchestrateur> creating ipv4 socket ...\n");
			sleep(rand()%2);
	    printf("orchestrateur> ipv4 socket created ...\n");

	}

	addr_len=sizeof(struct sockaddr_in6);
	bzero(&addr,sizeof(addr));
	addr.sin6_family=AF_INET6;
	inet_pton(AF_INET6,REMOTEIP,&addr.sin6_addr);

	addr_len2=sizeof(struct sockaddr_in);
	bzero(&addr2,sizeof(addr2));
	addr2.sin_family=AF_INET;
	inet_pton(AF_INET,REMOTEIP,&addr2.sin_addr);

	char op;
	int iop;
	int len;
	struct timeval read_timeout;
	read_timeout.tv_sec = 0;
	read_timeout.tv_usec = 10;
	printf("orchestrateur> press enter to validate the line\n");
	printf("orchestrateur> press enter with an empty line to validate and start processing\n");
	printf("orchestrateur> write exit to exit\n");
  while(1)
  {
		aff();
      bzero(msg,sizeof(msg));
		int i=0;
		int i2=0;
		int boolean = 0;
		int boolean2 = 0;
		while((write(STDIN_FILENO,"orchestrateur> ",sizeof("orchestrateur> "))!=0))
		{
			len=read(STDIN_FILENO,msg,sizeof(msg));
			if(len<=1)
			{
				break;
			}
			if(strncmp(msg,"exit",4)==0)
			{
				printf("orchestrateur> exit...\n");
				return(0);
			}

			if(i<200)
			{
				int j=0;
				while(msg[j]!='\n')
					j++;
				msg[j]='\0';
				//NEW CODE
				int test=0;
				unsigned int init;
				while(init<sizeof(msg)-1)
				{
					if(((msg[init]=='+')||(msg[init]=='-')||(msg[init]=='*')||(msg[init]=='/'))&&(msg[init+1]=='('))
					{
						test++;
						if(test>1)
						{
							boolean2=1;
							printf("orchestrateur> too much requests in one\n");
							break;
						}
					}
					init++;
				}
				if(boolean2==1)
					break;
				//END NEW CODE
				else
				{
					op = msg[0];
					if(op=='+')
					{
						strcpy(msg2[i][0],msg);
						i++;
					}
					if(op=='-')
					{
						strcpy(msg2[i][1],msg);
						i++;
					}
					if(op=='*')
					{
						strcpy(msg2[i][2],msg);
						i++;
					}
					if(op=='/')
					{
						strcpy(msg2[i][3],msg);
						i++;
					}
				}
				boolean=0;
			}
			else
			{
				printf("orchestrateur> Too much requests, processing all but not %s ...\n",msg);
				break;
			}
			bzero(msg,sizeof(msg));
		}
		int k;
		int f;
		for(k=0;k<4;k++)
		{
			if(strcmp(msg2[0][k],"")!=0)
				boolean=1;
		}
		if(boolean==1)
		{
			printf("orchestrateur> processing request(s) ...\n");
		}
		boolean=0;
		for(k=i-1;k>-1;k--)
		{

			if(strcmp(msg2[k][0],"")!=0)
			{
				op='+';
				iop=0;
				addr.sin6_port=htons(addPORT);
				addr2.sin_port=htons(addPORT);
				port=addPORT;
			}
			if(strcmp(msg2[k][1],"")!=0)
			{
				op='-';
				iop=1;
				addr.sin6_port=htons(minPORT);
				addr2.sin_port=htons(minPORT);
				port=minPORT;
			}
			if(strcmp(msg2[k][2],"")!=0)
			{
				op='*';
				iop=2;
				addr.sin6_port=htons(mulPORT);
				addr2.sin_port=htons(mulPORT);
				port=mulPORT;
			}
			if(strcmp(msg2[k][3],"")!=0)
			{
				op='/';
				iop=3;
				addr.sin6_port=htons(divPORT);
				addr2.sin_port=htons(divPORT);
				port=divPORT;
			}
			printf("orchestrateur> sending %s to socket ...\n", msg2[k][iop] );
			time_t sendnow;
			time (&sendnow);
			strcpy(sendtime1,ctime(&sendnow));
			setsockopt(mysocket,SOL_SOCKET,SO_RCVTIMEO, &read_timeout,sizeof read_timeout);
			if((sendto(mysocket,msg2[k][iop],sizeof(msg2[k][iop]),0,(struct sockaddr *)&addr,addr_len))<0)
			{
				printf("orchestrateur> the ipv6 node '%c' is not available\n",op);
				printf("orchestrateur> trying the ipv4 node...\n");
				boolean=1;
			}
			sleep(rand()%10);
			if((recvfrom(mysocket,msg2[k][iop],sizeof(msg2[k][iop]),0,(struct sockaddr *)&addr,(socklen_t*)&addr_len))<0)
			{
				printf("orchestrateur> the ipv6 node '%c' is not available\n",op);
				printf("orchestrateur> trying the ipv4 node...\n");
				boolean=1;
			}
			else
			{
				printf("orchestrateur> the ipv6 node '%c' is available\n",op);
				printf("orchestrateur> not trying the ipv4 node\n");
			}
			if(boolean==1)
			{
				boolean=0;
				time_t sendnow3;
				time (&sendnow3);
				strcpy(sendtime1,ctime(&sendnow3));
				setsockopt(mysocket2,SOL_SOCKET,SO_RCVTIMEO, &read_timeout,sizeof read_timeout);
				if((sendto(mysocket2,msg2[k][iop],sizeof(msg2[k][iop]),0,(struct sockaddr *)&addr2,addr_len2))<0)
				{
					printf("orchestrateur> the ipv4 node '%c' is not available\n",op);
					printf("orchestrateur> proceding without the missing node...\n");
					boolean=1;
				}
				sleep(rand()%10);
				if((recvfrom(mysocket2,msg2[k][iop],sizeof(msg2[k][iop]),0,(struct sockaddr *)&addr2,(socklen_t*)&addr_len2))<0)
				{
					printf("orchestrateur> the ipv4 node '%c' is not available\n",op);
					printf("orchestrateur> proceding without the missing node...\n");
					boolean=1;
				}
				else
				{
					printf("orchestrateur> the ipv4 node '%c' is available\n",op);
				}

			}
			if(boolean==0)
			{
				time_t recvnow;
				time (&recvnow);
				strcpy(recvtime2,ctime(&recvnow));
				char str[26];
				char str2[26];
				strncpy(str, sendtime1 + 11, 8);
				str[8]='\0';
				strncpy(str2, recvtime2 + 11, 8);
				str2[8]='\0';
		    printf("orchestrateur> Received message :\n");
				printf("sended at %s by <%s,%d> recieved at %s\n",str,REMOTEIP,port,str2);
				printf("orchestrateur> %s\n",msg2[k][iop]);
			}
			else
			{
				strcpy(msgerr[iop][i2],msg2[k][iop]);
				i2++;
			}
		}

		if(i2>0)
		{
			for(f=0;f<4;f++)
			{
				if(strcmp(msgerr[f][0],"")!=0)
				{
					if(f==0)
						op='+';
					if(f==1)
						op='-';
					if(f==2)
						op='*';
					if(f==3)
						op='/';
					printf("orchestrateur> The node '%c' is missing, the following message(s) has not been send :\n",op);
					for(k=i2-1;k>-1;k--)
					{
						printf("orchestrateur> %s\n", msgerr[f][k]);
					}
				}
			}
		}
		printf("\n");

		for(k=0;k<i;k++)
		{
			for(f=0;f<4;f++)
				bzero(msg2[k][f],sizeof(msg2[k][f]));
		}
		for(f=0;f<4;f++)
		{
			for(k=0;k<i2;k++)
				bzero(msgerr[f][k],sizeof(msgerr[f][k]));
		}

		sleep(rand()%2);
	}
}
