#!/bin/bash

#traite les entrées du scripte
if  [ $# -eq 1 ] && [ $# -ne 0 ] && [ $# -ne 5 ]
then
  if [ $# -eq 1 ]
  then
    if [ $1 != 'clean' ] && [ $1 != 'mrproper' ] && [ $1 != 'make' ] && [ $1 != 'random' ] && [ $1 != 'notallnodes' ]
    then
      echo "USAGE: $0 clean"
      echo "or"
      echo "USAGE: $0 mrproper"
      echo "or"
      echo "USAGE: $0 make"
      echo "or"
      echo "USAGE: $0 ((IP and ports) or random)"
      exit 1
    fi
  else
    echo "USAGE: $0 clean"
    echo "or"
    echo "USAGE: $0 mrproper"
    echo "or"
    echo "USAGE: $0 make"
    echo "or"
    echo "USAGE: $0 ((IP and ports) or random)"
    exit 1
  fi
fi

if [ $# -eq 1 ]
then
  #fait un make sur l'orchestrateur et les noeuds
  if [ $1 == 'make' ]
  then
    cd orchestrateur
    make
    cd ../nodeipv6/add
    make
    cd ../div
    make
    cd ../min
    make
    cd ../mul
    make
    cd ../../nodeipv4/add
    make
    cd ../div
    make
    cd ../min
    make
    cd ../mul
    make
    cd ../..
    exit 0
  fi

  #fait un make clean sur l'orchestrateur et les noeuds
  if [ $1 == 'clean' ]
  then
    cd orchestrateur
    make clean
    cd ../nodeipv6/add
    make clean
    cd ../div
    make clean
    cd ../min
    make clean
    cd ../mul
    make clean
    cd ../../nodeipv4/add
    make clean
    cd ../div
    make clean
    cd ../min
    make clean
    cd ../mul
    make clean
    cd ../..
    exit 0
  fi

  #fait un make mprproper sur l'orchestrateur et les noeuds
  if [ $1 == 'mrproper' ]
  then
    cd orchestrateur
    make mrproper
    cd ../nodeipv6/add
    make mrproper
    cd ../div
    make mrproper
    cd ../min
    make mrproper
    cd ../mul
    make mrproper
    cd ../../nodeipv4/add
    make mrproper
    cd ../div
    make mrproper
    cd ../min
    make mrproper
    cd ../mul
    make mrproper
    cd ../..
    exit 0
  fi
fi

#lancement de l'orchestrateur et des noeuds
#dans toutes ces boucles de lancement,
#le scripte lance aléatoirement les noeuds en ipv4 ou ipv6
if [ -f ./orchestrateur/orchestrateur ]
then
  #fonction permettant de lancer les noeuds en arrière plan
  evalBack() { eval "$@" &>/dev/null &disown; }

  rand01=$(grep -m1 -ao '[0-9]' /dev/urandom | sed s/0/10/ | head -n1)
  rand02=$(grep -m1 -ao '[0-9]' /dev/urandom | sed s/0/10/ | head -n1)
  rand03=$(grep -m1 -ao '[0-9]' /dev/urandom | sed s/0/10/ | head -n1)
  rand04=$(grep -m1 -ao '[0-9]' /dev/urandom | sed s/0/10/ | head -n1)
  rand1=$(($rand01%2))
  rand2=$(($rand02%2))
  rand3=$(($rand03%2))
  rand4=$(($rand04%2))
  #lancement des noeuds et de l'orchestrateur avec l'adresse et les ports en paramètres
  if [ $# -eq 5 ]
  then
    if [ "$rand1" -eq "1" ]
    then
      evalBack ./nodeipv6/add/node_add $2
    else
      evalBack ./nodeipv4/add/node_add $2
    fi

    if [ "$rand2" -eq "1" ]
    then
      evalBack ./nodeipv6/div/node_div $5
    else
      evalBack ./nodeipv4/div/node_div $5
    fi

    if [ "$rand3" -eq "1" ]
    then
      evalBack ./nodeipv6/min/node_min $3
    else
      evalBack ./nodeipv4/min/node_min $3
    fi

    if [ "$rand4" -eq "1" ]
    then
      evalBack ./nodeipv6/mul/node_mul $4
    else
      evalBack ./nodeipv4/mul/node_mul $4
    fi

    ./orchestrateur/orchestrateur "$1" $2 $3 $4 $5

  #lancement des noeuds et de l'orchestrateur avec l'adresse et les ports de défaut sur turing
  elif [ $# -eq 0 ]
  then

    if [ "$rand1" -eq "1" ]
    then
      evalBack ./nodeipv6/add/node_add
    else
      evalBack ./nodeipv4/add/node_add
    fi

    if [ "$rand2" -eq "1" ]
    then
      evalBack ./nodeipv6/div/node_div
    else
      evalBack ./nodeipv4/div/node_div
    fi

    if [ "$rand3" -eq "1" ]
    then
      evalBack ./nodeipv6/min/node_min
    else
      evalBack ./nodeipv4/min/node_min
    fi

    if [ "$rand4" -eq "1" ]
    then
      evalBack ./nodeipv6/mul/node_mul
    else
      evalBack ./nodeipv4/mul/node_mul
    fi

    ./orchestrateur/orchestrateur

  #lancement de l'orchestrateur et des noeuds avec l'adresse de défaut sur turing et des ports aléatoires
  elif [ $# -eq 1 ]
  then
    if [ $1 == 'random' ]
    then
      portadd=$(grep -m1 -ao '[2-7][0-9][0-9][0-9]' /dev/urandom | sed s/2000/7999/ | head -n1)

      portmin=$portadd

      while [ $portmin -eq $portadd ]
      do
        portmin=$(grep -m1 -ao '[2-7][0-9][0-9][0-9]' /dev/urandom | sed s/2000/7999/ | head -n1)
      done

      portmul=$portmin

      while [ $portmul -eq $portadd ] || [ $portmul -eq $portmin ]
      do
        portmul=$(grep -m1 -ao '[2-7][0-9][0-9][0-9]' /dev/urandom | sed s/2000/7999/ | head -n1)
      done

      portdiv=$portmul

      while [ $portdiv -eq $portadd ] || [ $portdiv -eq $portmin ] || [ $portdiv -eq $portmul ]
      do
        portdiv=$(grep -m1 -ao '[2-7][0-9][0-9][0-9]' /dev/urandom | sed s/2000/7999/ | head -n1)
      done

      if [ "$rand1" -eq "1" ]
      then
        evalBack ./nodeipv6/add/node_add $portadd
      else
        evalBack ./nodeipv4/add/node_add $portadd
      fi

      if [ "$rand2" -eq "1" ]
      then
        evalBack ./nodeipv6/div/node_div $portdiv
      else
        evalBack ./nodeipv4/div/node_div $portdiv
      fi

      if [ "$rand3" -eq "1" ]
      then
        evalBack ./nodeipv6/min/node_min $portmin
      else
        evalBack ./nodeipv4/min/node_min $portmin
      fi

      if [ "$rand4" -eq "1" ]
      then
        evalBack ./nodeipv6/mul/node_mul $portmul
      else
        evalBack ./nodeipv4/mul/node_mul $portmul
      fi

      ./orchestrateur/orchestrateur "turing6.u-strasbg.fr" $portadd $portmin $portmul $portdiv
    fi
    if [ $1 == 'notallnodes' ]
    then
      rand1=$(($rand01%3))
      rand2=$(($rand02%3))
      rand3=$(($rand03%3))
      rand4=$(($rand04%3))
      if [ "$rand1" -eq "1" ]
      then
        evalBack ./nodeipv6/add/node_add
      elif  [ "$rand1" -eq "0" ]
      then
        evalBack ./nodeipv4/add/node_add
      fi

      if [ "$rand2" -eq "1" ]
      then
        evalBack ./nodeipv6/div/node_div
      elif  [ "$rand2" -eq "0" ]
      then
        evalBack ./nodeipv4/div/node_div
      fi

      if [ "$rand3" -eq "1" ]
      then
        evalBack ./nodeipv6/min/node_min
      elif  [ "$rand2" -eq "0" ]
      then
        evalBack ./nodeipv4/min/node_min
      fi

      if [ "$rand4" -eq "1" ]
      then
        evalBack ./nodeipv6/mul/node_mul
      elif  [ "$rand2" -eq "0" ]
      then
        evalBack ./nodeipv4/mul/node_mul
      fi

      ./orchestrateur/orchestrateur
    fi
  fi
#cas où l'orchestrateur n'as pas été compilé
else
  echo "USAGE: 'make' before launching the program"
  exit 1
fi

#assassinat des processus des noeuds
if [ $# -eq 0 ] || [ $# -eq 5 ] || [ $# -eq 1 ]
then
  if [ $# -eq 1 ]
  then
    if [ $1 == 'random' ]
    then
      killall -9 node_add
      killall -9 node_div
      killall -9 node_min
      killall -9 node_mul
    elif [ $1 == 'notallnodes' ]
    then
      if [ "$rand1" -ne "2" ]
      then
        killall -9 node_add
      fi
      if [ "$rand2" -ne "2" ]
      then
        killall -9 node_div
      fi
      if [ "$rand3" -ne "2" ]
      then
        killall -9 node_min
      fi
      if [ "$rand4" -ne "2" ]
      then
        killall -9 node_mul
      fi
    fi
  else
    killall -9 node_add
    killall -9 node_div
    killall -9 node_min
    killall -9 node_mul
  fi
  exit 0
fi
