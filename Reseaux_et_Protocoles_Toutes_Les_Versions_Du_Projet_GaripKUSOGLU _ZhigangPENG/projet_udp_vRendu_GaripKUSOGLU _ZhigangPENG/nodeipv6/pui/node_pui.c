#include <unistd.h>
#include <stdio.h>
#include <arpa/inet.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <string.h>
#include <time.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <limits.h>

int main(int argc, char **argv)
{
  //initialisation du port
  int LOCALPORT = 6005;
	if(argc != 2)
	{
		printf("USAGE: %s LOCALPORT\n", argv[0]);
		printf("proceding with default port\n");
	}
	else
	{
    //affectation de l'argument en paramètre au port
    LOCALPORT = atoi(argv[1]);
  }
  int mysocket,len;
  int num,res,num2=1;
  float res3,res2;

  struct sockaddr_in6 addr;
  int addr_len,i=0,q;
  char msg[200];
  char buf[300];

  //creation de la socket
  if((mysocket=socket(AF_INET6,SOCK_DGRAM,0))<0)
  {
    perror("error:");
    return(1);
  }
  else
  {
    printf("socket created ...\n");
    printf("socket id :%d \n",mysocket);
  }

  addr_len=sizeof(struct sockaddr_in6);
  bzero(&addr,sizeof(addr));
  addr.sin6_family=AF_INET6;
  addr.sin6_port=htons(LOCALPORT);
  addr.sin6_addr=in6addr_any;

  if(bind(mysocket,(struct sockaddr *)&addr,sizeof(addr))<0)
  {
      perror("connect");
      return(1);
  }
  else
  {
      printf("bind ok .\n");
      printf("local port : %d\n",LOCALPORT);
  }

  //boucle de reception et d'envoi des messages
  while(1)
  {
    i=0;
    num2=1;
    bzero(msg,sizeof(msg));
    //reception du calcul
    len = recvfrom(mysocket,msg,sizeof(msg),0,(struct sockaddr *)&addr,(socklen_t*)&addr_len);
    printf("reading numbers...");

    //boucle de calcul du resultat
    while(msg[i]&&(msg[i]<'0' || msg[i]>'9' ))
      i++;
    if(msg[i])
    {
      //p=i;
      q=i+1;
      res=msg[i]-'0';
      if(i>=1 && msg[i-1]=='-')
        res=-res;
      while(msg[q]>='0' && msg[q]<='9')
      {
        res=10*res+(msg[q]-'0');
        q++;
      }
      i=q;
    }
    while(1)
    {
      while(msg[i]&&(msg[i]<'0' || msg[i]>'9' ))
        i++;
      if(msg[i])
      {
        //p=i;
        q=i+1;
        num=msg[i]-'0';
        if(i>=1 && msg[i-1]=='-')
          num=-num;
        while(msg[q]>='0' && msg[q]<='9')
        {
          num=10*num+(msg[q]-'0');
          q++;
        }
        i=q;
      }
      else
        break;
      num2*=num;
    }
    res2=(float)res;
    if(num2==0)
    	res2=1.0;
    if(num2<0)
    {
      res2=1.0;
    	res3=1.0/(float)res;
    	while(num2!=0)
    	{
    		res2*=res3;
    		num2++;
    	}
    }
    if(num2>1)
    {
      res2=1.0;
    	while(num2!=0)
    	{
    		res2*=(float)res;
    		num2--;
    	}
    }

    sprintf(msg, "%s = %f",msg,res2);

    inet_ntop(AF_INET6,&addr.sin6_addr,buf,sizeof(buf));
    printf("message from ip %s\n",buf);
    printf("Received message : %s\n",msg);

    //envoi du résultat du calcul
    if(sendto(mysocket,msg,len,0,(struct sockaddr *)&addr,addr_len)<0)
    {
        printf("error");
        return(1);
    }
  }
}
